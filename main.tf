provider "aws" {
  region = "eu-west-2"
  assume_role {
    # Role ARN required for deployment
    role_arn = ""
  }
}
locals {
  dns_records = flatten([
    for z in var.zone_data : [
      for r in z.records : {
        zone_name = z.zone_name
        record    = r
      }
    ]
  ])
}

resource "aws_route53_zone" "zone" {
  for_each = {
    for z in var.zone_data : z.zone_name => z
  }

  name    = each.value.zone_name
  comment = each.value.comment
  dynamic "vpc" {
    for_each = each.value.vpc[*]
    content {
      vpc_id = each.value.vpc.vpc_id
    }
  }
  delegation_set_id = lookup(each.value, "delegation_set_id", null)
}

resource "aws_route53_record" "record" {
  for_each = {
    for dr in local.dns_records : "${dr.record.name}.${dr.zone_name} ${dr.record.type}" => dr
  }
  dynamic "alias" {
    for_each = each.value.record.alias[*]

    content {
      name                   = each.value.record.alias.target
      zone_id                = each.value.record.alias.target_zone
      evaluate_target_health = each.value.record.alias.evaluate_target_health
    }
  }
  zone_id = aws_route53_zone.zone[each.value.zone_name].id

  name    = each.value.record.name
  type    = each.value.record.type
  ttl     = each.value.record.ttl == null ? null : each.value.record.ttl
  records = each.value.record.record_set == null ? null : each.value.record.record_set
}

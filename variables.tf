variable "zone_data" {
  type = list(object({
    zone_name         = string
    comment           = string
    delegation_set_id = string
    vpc = object({
      vpc_id = string
      }
    )
    records = list(object({
      name       = string
      type       = string
      ttl        = number
      record_set = list(string)
      alias = object({
        evaluate_target_health = bool
        target                 = string
        target_zone            = string
      })
    }))
  }))
  default = [    
    {
      zone_name         = ""
      vpc               = null
      comment           = null
      delegation_set_id = null
      records = [
        {
          name       = ""
          type       = ""
          record_set = []
          alias      = null
          ttl        = null
          record_set = null
        }
      ]
    }
  ]
}

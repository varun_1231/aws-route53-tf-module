zone_data = [
  {
    zone_name = "trulogis.com"
    comment   = "test"
    vpc = {
      vpc_id = "vpc-03505855f0d8ac2ef"
    }
    delegation_set_id = null
    records = [{
      name       = "www"
      record_set = null
      ttl        = null
      alias = {
        evaluate_target_health = true
        target                 = "test.us-west-2.elb.amazonaws.com."
        target_zone            = "Z1234K"
      }
      type = "A"
    }]
  },
  {
    zone_name         = "test.com"
    comment           = "test zone creation"
    vpc               = null
    delegation_set_id = "ZBXVGDXD"
    records = [{
      name = "admin",
      record_set = [
        "2.2.2.2"
      ],
      ttl   = 3600,
      alias = null,
      type  = "A"
    }]
  }
]

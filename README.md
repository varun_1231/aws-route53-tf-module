# aws-route53-tf-module

Request for optional arguments in object variable type definition.


https://github.com/hashicorp/terraform/issues/19898


When the arguments are defaulted to null, error is as below:

```Error: Invalid value for input variable

  on test.tfvars line 1: <br />
   1: zone_data = [ <br />
   2:   {<br />
   3:     zone_name = "trulogis.com"<br />
   4:     comment   = "test"<br />
   5:     vpc = {<br />
   6:       vpc_id = "vpc-03505855f0d8ac2ef"<br />
   7:     }<br />
   8:     records = [{<br />
   9:       name       = "www"<br />
  10:       record_set = null<br />
  11:       ttl        = null<br />
  12:       alias = {<br />
  13:         evaluate_target_health = true<br />
  14:         target                 = "test.us-west-2.elb.amazonaws.com."<br />
  15:         target_zone            = "Z1234K"<br />
  16:       }<br />
  17:       type = "A"<br />
  18:     }]<br />
  19:   },<br />
  20:   {<br />
  21:     zone_name         = "test.com"<br />
  22:     comment           = "test zone creation"<br />
  23:     vpc               = null<br />
  24:     delegation_set_id = "ZBXVGDXD"<br />
  25:     records = [{<br />
  26:       name = "admin",<br />
  27:       record_set = [<br />
  28:         "2.2.2.2"<br />
  29:       ],<br />
  30:       ttl   = 3600,<br />
  31:       alias = null,<br />
  32:       type  = "A"<br />
  33:     }]<br />
  34:   }<br />
  35: ]<br />
<br />
The given value is not valid for variable "zone_data": element 0: attribute<br />
"delegation_set_id" is required.
```
